# Écoconception

Réflexion autour de l’évolution technique, et l'écosystème et des efforts à faire

---

## Le point de départ

> "De toutes façons, parler d'éco-conception alors qu'on fait tourner 800 000 machines là où y'a 15 ans on avait un pauvre serveur FTP, hein..."

<small>-- un collègue<small>

--

### D'où parles-tu, camarade ?

* [pand.app](https://pand.app) (♥️ le pseudonymat)
* En activité depuis 2008
* Digresseur pro
* Président de [Qonfucius](https://qonfucius.com)
* [Qongzi](https://qongzi.com) par [Qonfucius](https://qonfucius.com)
* Membre du [Mouvement Impact France](https://www.impactfrance.eco/)
* Membre accompagnateur [Réseau Entreprendre](https://www.reseau-entreprendre.org/) 91
* 100% télétravail
* Pas de babyfoot

Note:
Présenter MIF et RE

---

## Contexte d'évolution

Mises en perspectives

--

### Évolution démographique
* Année 2000
  * 6 100 000 000 humains
  * 1 023 000 000 internautes (2005)
* Année 2010
  * 6 900 000 000 humains
  * 1 981 000 000 internautes
* Année 2022
  * ~ 8 000 000 000 humains
  * ~ 5 300 000 000 internautes

<small>Source : Wikipedia</small>

--

### Évolution du modèle de menaces

* Année 2000
  * ILOVEYOU
  * Attaques ciblées
* Année 2010
  * Logique de marché
  * "Pêche au chalutier"
* Aujourd'hui (2022)
  * Automatisation
  * Ransomware
  * Attaques Etatiques & militarisation

<small>Source : <a href="https://securitybrief.co.nz/story/a-brief-history-of-cyber-threats-from-2000-to-2020">Sophos</a></small>

Note:
Ver ayant infecté plus de 10 Millions d'ordinateurs windows depuis une PJ "LOVE-LETTER-FOR-YOU.TXT.vbs" à partir du 5 Mai 2000 ; réécrit des fichiers au hazard. Auteur présumé Onel de Guzman, 24 ans.

--

### Évolution du modèle de navigation

* 2007 : iPhone
* 2011 : 1 775 000 000 mobiles vendus
* 2022 : 8 580 000 000 de smartphones (?) en circulation

<small>Sources : Wikipedia, <a href="https://www.phonandroid.com/il-y-a-859-milliards-de-smartphones-en-circulation-dans-le-monde-plus-que-dhumains.html">Phonandroid</a>, <a href="https://fr.statista.com/statistiques/559525/prevision-nombre-dutilisateurs-mobiles-dans-le-monde-2010-2020/">Statista</a>, <a href="https://www.zdnet.fr/actualites/chiffres-cles-les-ventes-de-mobiles-et-de-smartphones-39789928.htm?p=4">ZDNet</a></small>

--

### Évolution de la puissance

* 2000 : 400Mhz & 256Mo RAM
* 2010 : 3200Mhz & 4096Mo RAM
* 2022 : 4600MHz & 32768Mo RAM

<small>Sources : oskour</small>

--

### Évolution des connexions internet

* 2000 : 1.5Mbps
* 2010 : 8Mbps
* 2022 : 1000Mbps

<small>Sources : oskour</small>

--

### Évolution des mentalités

* ⤴️ des attentes quant à la disponibilité d'un service
* ⤴️ des pertes en cas de rupture de service
* ⤴️ des attentes de rapidité ?
* ⤴️ des attentes d'ownership
* ⤴️ de la taille des équipes dev

Note:
Evolution des usages
Internet partout..

---

## Le déni

C'est une goutte d'eau

Note:
Le déni > La colère > Le marchandage > La dépression > L'acceptation & l'action.

--

### Faisons un tour sur https://www.ecologie.gouv.fr/

* 16.9 Mo d'empreinte mémoire
* 28 Requêtes 
* 2.78 MB / 1.64 MB transféré
* 949 ms

<small>(Et en bonus, des requêtes à Twitter, Cloudflare, Edgecast, et un cookie identifiant)</small>

<small>Source : Panneau réseau de Firefox</small>

Note:
Cet exemple est intéressant car il n'est pas WTF

--

### DNS 

* **www.ecologie.gouv.fr**
  * root -> nic.fr -> ns developpement-durable -> résolution
* **hlms.ecologie.gouv.fr**
  * root -> nic.fr -> ns developpement-durable -> résolution
* **platform.twitter.com**
  * root -> gtld-servers -> Twitter DNS -> résolution
* **cdnjs.cloudflare.com**
  * root -> gtld-servers -> cloudflare DNS -> résolution

*12 serveurs DNS impliqués (en réalité plus **et** moins)*

<small>Source : dig +noadditional +noquestion +nocomments +nocmd +nostats +trace |domain| @8.8.4.4</small>

Note:
Cache ; "Serveur DNS faisant autorité" (bortzmeyer.org) -> Ecoindex A ; "Résolveur DNS : définition" (bortzmeyer.org)

--

### Routage

* **www.ecologie.gouv.fr**
  * 9+ sauts
* **hlms.ecologie.gouv.fr**
  * 8+ sauts
* **platform.twitter.com**
  * 7 sauts
* **cdnjs.cloudflare.com**
  * 9 sauts

*Au moins 33 matériels réseau impliqués*

<small>Source : traceroute |domain|</small>

Note:
Asymétrie réseau, opacité réseau

--

### Frameworks

* Drupal
* Chart.js
* jQuery
* Autres ?

<small>Source : code source de la page</small>

Note:
And so what ?

--

### Axes d'amélioration

* 20% de ressources compressables
* 2 ressources à mettre en cache
* 1 image mise à l'échelle par le navigateur
* 4 inline à externaliser
* 21 ressources statiques embarquent un cookie
* 2 images à optimiser
* 6 ressources en HTTP/1
* 4 typographies chargées

<small>Source : plugin navigateur Green IT</small>

--

### Râler sur…

* Les antinucléaires
* Les gens qui prennent l'avion
* La surconsommation
* La viande
* Le voisin qui arrose sa pelouse à 12h
* Les pièces jointes rigolotes
* Le capitalisme
* …

<small>Source : Moi, chaque jour de l'année, n'hésitez pas à me demander d'autres sujets de râlage</small>

---

## Mesurer l'impact, c'est compliqué

--

### Il faut penser à…

* Consommation d'eau
* Impact social de l'extraction
* Impact social de la fabrication
* Impact social du rebus
* Biodiversité
* Rejets GES
* Effets rebonds
* Durée de vie
* Mix énergétique
* Chaque équipement impliqué
* Pipi sous la douche
* …

Note:
6e rapport du GIEC : justice sociale

B. 4.1, résumé aux décideurs : **L'efficacité de l'adaptation, y compris celle basée sur les écosystèmes et la plupart des options liées à l'eau, diminuera à mesure que le réchauffement augmentera. La faisabilité et l'efficacité des options augmentent avec des solutions intégrées, multisectorielles qui différencient les réponses en fonction du risque climatique, traversent les systèmes et abordent les inégalités sociales.**

--

### Imputer l'impact à l'utilisateur·trice

* Iel est aveugle à l'utilisation du matériel
* Iel est aveugle au type de matériel
* Iel est aveugle à la quantité de matériel

--

### Imputer l'impact au propriétaire

* Location
* Les actions d'une entreprise

Note:
Quid d'imputer l'impact au fabriquant ?

--

### Qui peut agir sur quoi ?

| | Utilisateur | Editeur | Hébergeur |
|-|-------------|---------|--------------|
| ⚡️ terminal | ✅ | ✅ | ❌ |
| Volume données | ✅ | ✅ | ❌ |
| ♻️ terminal | ✅ | ✅ | ❌ |
| ⚡️ serveur | ❌ | ✅ | ✅ |
| ♻️ serveur | ❌ | ✅ | ✅ |
| 🗺 localisation️ | ❌ | ✅ | ❌ |
| 🏫 formation | ✅ | ✅ | ✅ |

--

### Renouvellement

* Delta de consommation entre l'ancien et le nouveau
* La fréquence d'utilisation
* La durée de vie
* Impact environnemental de la production
* Recyclage

Note:
Exemple de l'💡 de Livermore

Voiture électrique VS thermique

--

### Effets rebond

- Email
- Streaming
- Dark pattern ?

Note:
Expliquer ce qu'est l'effet rebond

---

## Digression : le point ADEME

<small>Préambule : Ne jetez pas bébé avec l'eau du bain</small>

L'ADEME a commandé, en 2011, une analyse comparée des impacts environnementaux de la communication par voie électronique. Un rapport de BIO Intelligence Service (maintenant Deloitte) de 44 pages a été publié.

-> Méthode scientifique ?

<small>[Rapport "Presse" de l'ADEME](https://presse.ademe.fr/files/acv_ntic_synthese_resultats.pdf)</small>

--

### Paramètres étudiés (Mail)

* Taille du mail (PJ incluse)
* Durée d'affichage à l'écran
* Impression par le destinataire
* Nombre de destinataires
* Durée de stockage du mail

<small>[Rapport "Presse" de l'ADEME](https://presse.ademe.fr/files/acv_ntic_synthese_resultats.pdf)</small>

--

### Paramètres étudiés (Recherche)

* Durée de vie du matériel
* Temps passé par page
* Nombre de pages étudiées
* Utilisation "Favoris"

<small>[Rapport "Presse" de l'ADEME](https://presse.ademe.fr/files/acv_ntic_synthese_resultats.pdf)</small>

--

### Paramètres étudiés (Clef USB)

* Taille clef USB
* Composition coque
* Typologie de document
* Temps de lecture
* Impression
* ...

<small>[Rapport "Presse" de l'ADEME](https://presse.ademe.fr/files/acv_ntic_synthese_resultats.pdf)</small>

--

### Critiques

* Accès aux données brutes ?
* Détail des calculs suivants les méthodes *CML & IMPACT 2002+* et *ReCiPe* ?

Note:
RGPD : XITI, AWS, Wordpress

--

### À garder de ce rapport ?

Le réseau représenterait < 5% de l'impact

<small>Donc je peux continuer à répondre "Merci" en text/plain par mail.</small>

--

### Bonus : Mesurer la consommation de votre IT ?

* [Scaphandre](https://github.com/hubblo-org/scaphandre) (requiert intel_rapl 👀)
* Tableur
* Green IT
* codecarbon.io
* PowerAPI

Note:
Si seulement Scaleway avait intel_rapl sur les workers Kubernetes

---

## Revenons à nos moutons

C'était sympa, mais le point de départ parlait de DevOps, de CI/CD...

--

### Le devOps chez nous

* Souveraineté
* Clusters Kubernetes
* IaC Terraform + Helm
* Gitlab CI
* (OCI)

Note:
IaC Opensource secteur public

--

### Le dev chez nous

* Stack JS
* Rust

--

### Notre cible

![Graphique type quadrants sur deux axes : Faible vers haute efficacité énergétique et simplicité vers haute technologie. Notre cible setrouve dans le qudrant Haute efficacité ET Haute technologie. Un haute spot important se trouve au delà de la haute efficacité et simplicité technologique : Ne rien faire.](./images/magicquadrant.svg)<!-- .element: height="600" -->

--

### Nos besoins

* Résilience
* Disponibilité
* Garanties
* Neutralité
* PRA & PCA
* Cybersécurité
* Immutabilité
* Reproductibilité

Note:
Institutionnel

--

### The good old days

A.k.a. mes premiers boulots de dev

* Passer 15 minutes à changer de branche sur SVN
* Me retrouver avec des fichiers *_backup* sur le FTP
* Réupload tout un dossier "au cas où"
* Ne pas savoir pourquoi ça fonctionne pas
* Oups, j'ai upload le fichier local de config

--

### Une pipeline type

1. Préparation & cache
2. Build
   * OCI & Crate
3. Tests
   * Scan conteneur & Dépendances & Analyses de code & Licenses & Recherche de secrets & Code quality & Lint & Convco
4. Déploiement
5. DAST
6. Performances
7. (Accessibilité)
8. (Compliance RGPD)

--

### Les garanties apportées

* Connaitre des tendances (perfs, qualité)
* Réduction de la surface d'attaque sous la responsabilité des devs
* Ne pas faire confiance au dev 👀
* Visibilité sur les vulnérabilités et faiblesses de sécurité
* Législatif
* Neutralité de reviewer

--

### Pistes mélioratives

* (Refuser des projets)
* Mutualisation
* Jobs manuels
* _Auto shutdown_
* _Interruptibles_
* Fine tuning des ressources
* <acronym title="Selective Continuous Integration">SCI</acronym>
* Scale down ! (~66%)
* Maitriser le parc ?
* Cache, et globalement tout ce qui fait gagner du temps de calcul

Note:
Dev : Tree shaking, cache, réduction images, lazy loading

--

### Shift left

* Build le plus possible en amont
* Compilé vs interprété
* WASM ?
* Renversement suivant le service rendu

---

## À vous !

<small>L'occasion de ne pas répondre à d'autres questions</small>

---

## Demain ?

* Diminuer l'impact plutôt qu'acheter des crédits carbones et/ou de la bonne conscience…
* <acronym title="Conventions industrielles de formation par la recherche">CIFRE</acronym>

Note:
Vote novembre 2022

---

## Merci !
